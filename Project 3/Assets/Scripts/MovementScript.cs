﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {

	public float playerSpeed;
	public float backSpeed;
	public float rotateSpeed;
	private float currentPlayerSpeed;
	private float currentBackSpeed;
	private float currentRotateSpeed;
	static float noiseLevel;
	public float currentNoiseLevel;
	CircleCollider2D soundSize;

	// Use this for initialization
	void Start () {
		noiseLevel = 0.25f;
		currentNoiseLevel = noiseLevel;
		soundSize = gameObject.GetComponent<CircleCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("left")) 
		{
			transform.Rotate (new Vector3 (0, 0, currentRotateSpeed));
			noiseLevel = 0.5f;
			currentNoiseLevel = noiseLevel;
			soundSize.radius = currentNoiseLevel * 2.5f; //sets audio size
		}
		if (Input.GetKey ("right")) 
		{
			transform.Rotate (new Vector3 (0, 0, currentRotateSpeed * -1));
			noiseLevel = 0.5f;
			currentNoiseLevel = noiseLevel;
			soundSize.radius = currentNoiseLevel * 2.5f; //sets audio size
		}
		if (Input.GetKey("down"))
		{
			transform.position += transform.up * currentBackSpeed * Time.deltaTime;
			noiseLevel = 0.75f;
			currentNoiseLevel = noiseLevel;
			soundSize.radius = currentNoiseLevel * 2.5f; //sets audio size
		}
		if (Input.GetKey ("up")) 
		{
			transform.position -= transform.up * currentPlayerSpeed * Time.deltaTime;
			noiseLevel = 1f;
			currentNoiseLevel = noiseLevel;
			soundSize.radius = currentNoiseLevel * 2.5f; //sets audio size
		}
		if (Input.GetKey ("left shift")) 
		{
			currentPlayerSpeed = 0.5f * playerSpeed;
			currentBackSpeed = 0.5f * backSpeed;
			currentRotateSpeed = 0.5f * rotateSpeed;
			currentNoiseLevel = noiseLevel * 0.5f;
			soundSize.radius = currentNoiseLevel * 2.5f; //sets audio size
		}
		else
		{
			currentPlayerSpeed = playerSpeed;
			currentBackSpeed = backSpeed;
			currentRotateSpeed = rotateSpeed;
		}
	}
}
