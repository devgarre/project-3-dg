﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionScript : MonoBehaviour {

	public GameObject winScreen;
	public GameObject gameOver;
	public GameObject currentGame;
	Vector3[] originalPosPlayer;
	Quaternion[] originalRotPlayer;
	Transform[] player;

	Vector3[] originalPosEnemy;
	Quaternion[] originalRotEnemy;
	Transform[] enemies;

	// Use this for initialization
	void Start () {
		backUpTransform ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Enemy") 
		{
			resetTransform ();
			gameOver.SetActive (true);
			currentGame.SetActive (false);
		}
		if (other.tag == "Objective") 
		{
			resetTransform ();
			winScreen.SetActive (true);
			currentGame.SetActive (false);
		}
	}

	//stores original character locations and rotation to reset after game ends
	void backUpTransform()
	{
		//stores enemy object locations
		GameObject[] tempEnemies = GameObject.FindGameObjectsWithTag ("Enemy");

		originalPosEnemy = new Vector3[tempEnemies.Length];
		originalRotEnemy = new Quaternion[tempEnemies.Length];

		enemies = new Transform[tempEnemies.Length];

		for(int i = 0; i < tempEnemies.Length; i++)
		{
			enemies [i] = tempEnemies [i].GetComponent<Transform> ();

			originalPosEnemy[i] = enemies[i].position;
			originalRotEnemy[i] = enemies[i].rotation;
		}

		//stores player object locations
		GameObject[] tempPlayer = GameObject.FindGameObjectsWithTag ("Player");

		originalPosPlayer = new Vector3[tempPlayer.Length];
		originalRotPlayer = new Quaternion[tempPlayer.Length];

		player = new Transform[tempPlayer.Length];

		for(int i = 0; i < tempPlayer.Length; i++)
		{
			player [i] = tempPlayer [i].GetComponent<Transform> ();

			originalPosPlayer[i] = player[i].position;
			originalRotPlayer[i] = player[i].rotation;
		}
	}

	//resets characters ot original locations and rotation
	void resetTransform()
	{
		for (int i = 0; i < enemies.Length; i++) 
		{
			enemies [i].position = originalPosEnemy [i];
			enemies [i].rotation = originalRotEnemy [i];
		}
		for (int i = 0; i < player.Length; i++) 
		{
			player [i].position = originalPosPlayer [i];
			player [i].rotation = originalRotPlayer [i];
		}
	}
}
