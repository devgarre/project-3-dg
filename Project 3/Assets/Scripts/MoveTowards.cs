﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowards : MonoBehaviour {

	private Transform tf;
	private Transform targetTf;
	private Vector3 movementVector;
	public float speed;
	public bool isSeeking;
	public float fieldOfView;

	// Use this for initialization
	void Start () {
		tf = GetComponent <Transform> ();
		targetTf = GameObject.Find("tank_huge").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		//if player is in view, the AI chases straight after them
		if (isSeeking) 
		{
			movementVector = targetTf.position - tf.position;
			tf.up = movementVector * -1;
		}
		movementVector.Normalize ();
		movementVector = movementVector * speed;
		tf.position = tf.position + movementVector;
		float angleToPlayer = Vector3.Angle (movementVector, transform.up * -1);
		if (angleToPlayer <= fieldOfView) {
			RaycastHit2D hitInfo = Physics2D.Raycast (tf.position, movementVector);
			if (hitInfo.collider.gameObject == GameObject.Find ("tank_huge")) {
				isSeeking = true;
			} else {
				isSeeking = false;
			}
		} 
		else 
		{
			isSeeking = false;
		}
	}

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.tag == "Player") {
			isSeeking = true;
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		if (other.tag == "Player") 
		{
			isSeeking = false;
		}
	}
}
